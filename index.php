<?php
require __DIR__.'/vendor/autoload.php';

use App\Post;
use App\Display\Message;

$post = new Post('Coucou');

$message = new Message();

$message->display($post, new DateTime());

$flash = new \App\Display\Flash();