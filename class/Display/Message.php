<?php


namespace App\Display;

use App\Post;
use DateTime;

/**
 * Class Message
 * @package App
 * Display the post title
 */
class Message
{
    public function display(Post $post, DateTime $date)
    {
        echo $post->getTitle();
    }
}

